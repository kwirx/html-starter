## Main Structure

####1. Base Mark - up

```html
<!doctype html>
<html>
    <body>
        <!-- Header content goes here, header wrappers can added on its own file for easy use -->
        <div class="wrap container" role="document">
            <div class="content row">
                <section class="content-header">
                    <!-- Header page content blocks goes here -->
                </section>
                <!-- Left sidebar blocks goes here, wrappers can added on its own file -->
                <main class="main">
                    <!-- Main Content Blocks Goes here  -->
                </main><!-- /.main -->
                <!-- Right sidebar blocks goes here, wrappers can added on its own file -->
                <section class="content-footer">
                    <!-- Header page content blocks goes here -->
                </section>
            </div><!-- /.content -->
        </div><!-- /.wrap -->
            <!-- Footer content goes here, header wrappers can added on its own file for easy use -->
    </body>
</html>
```

####2. Header Section

```html
<header class="banner">
    <div class="container">
        <div class="site-header">
            <!-- site header contents like logo, top navs, buttons social icons etc ..  -->
        </div>
        <!-- mani navigation based on bootstrap menu  -->
    </div>
</header>
```

####3. Footer Section

```html
<footer class="content-info">
    <div class="container">
        <!-- footer blocks, navs, credit/copytight text etc ..  -->
    </div>
</footer>
```

####4. Sidebar Section

```html
<aside class="sidebar sidebar-position">
    <!-- position is going to be left/right -->
    <!-- all sidebar modules from page builder go under this wrapper  -->
</aside>
```
5. Content Header Section
6. Content Footer Section


## Modules

### Main Content Area

####1. Custom Content Block
[Ref Image](http://dl.dropbox.com/u/33565642/content_block.png)

```html
<div class="content-block custom-content-block">
    <h2> <!-- title goes here  --> </h2>
    <div class="custom-content">
        <!-- content goes here -->
    </div>
</div>
```

####2. Content Grid
[Ref Image](http://dl.dropbox.com/u/33565642/child_grid.png)

```html
<div class="content-block custom-content-grid">
    <div class="content-grid row">
        <div class="conent-thumb col-sm-4 col-md-3 hidden-xs ">
            <a href="#">
                <img class="img-responsive img-rounded" src="#" alt="#">
                <!-- grid image goes hete  -->
            </a>
        </div>
        <div class="grid-content">
            <h3><a href="#"><!-- Grid title goes here --></a></h3>
            <!-- Grid content goes here  -->
            <a href="#">
                <!-- button details goes here  -->
            </a>
        </div>
    </div>
    <div class="content-grid row">
        ...
    </div>
</div>
```

####3. Child Page Grid
[Ref Image](http://dl.dropbox.com/u/33565642/child_grid.png)

```html
<div class="content-block child-page-grid">
    <div class="child-page content-grid row">
        <div class="conent-thumb col-sm-4 col-md-3 hidden-xs">
            <a href="#">
                <img class=" img-responsive img-rounded" src="#" alt="#">
            </a>
        </div>
        <div class="grid-content">
            <h3><a href="#"><!-- child page title goes here --></a></h3>
            <p>
                <!-- child page excerpt goes here  -->
            </p>
            <a href="#">Learn More »</a>
        </div>
    </div>
    <div class="child-page content-grid row">
        ...
    </div>
</div>
```

####4. Event Grid
[Ref Image](http://dl.dropbox.com/u/33565642/event_grid.png)

```html
<div class="content-block events-grid">
    <div class="events-row row">
        <div class="conent-thumb col-sm-4 col-md-3 hidden-xs ">
            <a href="#">
                <img class="img-responsive img-rounded" src="#" alt="#">
            </a>
        </div>
        <div class="grid-content">
            <a href="#"><h3><!-- event title goes here  --></h3></a>
            <div class="event-meta">
                <!-- event meta goes here  -->
            </div>
            <p>
                <!-- event excerpt goes here  -->
            </p>
            <a href="#">Find out more » </a>
        </div>
    </div>
    <div class="events-row row">
        ...
    </div>
</div>
```

####5. Section Title

```html
<h2 class="content-block section-title">
    <!-- section title goes here  -->
</h2>
```

####6. Separator Line

```html
<div class="content-block seperator clearfix"></div>
```

####7. Audio Library
[Ref Image](http://dl.dropbox.com/u/33565642/audio-library.png)

```html
<div class="content-block audio-library-block">
    <article class="single-audio clearfix row">
        <div class="audio-player span" data-toggle="modal" data-target="#aud_clip_ID">
            <div class="icon-bg"><i class="fa fa-play-circle"></i></div>
            <!-- play button code  -->
        </div>
        <div class="audio-details">
            <div class="audio-details-row">
                <div class="audio-content">
                    <h4>  <!-- clip title goes here  --></h4>
                    <span> <!-- clip date/meta goes here  -->  </span>
                    <div class="clip-description"> <!-- clip description goes here  -->  </div>
                </div>
                <div class="clip-download">
                    <a href="#" class="download-link btn" target="_blank">Download Clip</a>
                </div>
            </div>
        </div>
        <!-- popup window code and scripts for the audio player goes here  -->
    </article>
    <article class="single-audio clearfix row">
        ...
    </article>
    <!-- end of the audio lists items/loop  -->

    <footer>
        <nav class="pagination clearfix">
            <!-- pagination codes  -->
        </nav>
    </footer>
</div>
```

####8. Video Library
[Ref Image](http://dl.dropbox.com/u/33565642/video_library.png)

```html
<div class="content-block video-library-block">
    <div class="video-grid row ">
        <article class="single-video col-sm-4">
            <a href="#" rel="lightbox" title="#">
                <img src="#" alt="#video-thumb"></a>
            <aside>
                <h5 class="video-title"> <!-- video title goes here  --> </h5>
                <div class="video-description"> <!-- video description goes here  --> </div>
           </aside>
        </article>
        <article class="single-video col-sm-4">
            ...
        </article>
    </div>
    <footer>
        <nav class="pagination clearfix">
            <!-- pagination goes here  -->
        </nav>
    </footer>
</div>
```

####9. Image Gallery
[Ref Image](http://dl.dropbox.com/u/33565642/image-gallery.png)

```html
<div class="content-block photo-galleries">
    <div class="gallery-grid row">
        <article class="single-gallery col-sm-4">
            <a href="#single-gallery-link">
                <img src="#" alt="#gallery-thumb">
            </a>
            <aside>
                <h3> <!-- gallery title goes here --> </h3>
                <!-- gallery meta/description -->
            </aside>
        </article>
        <article class="single-gallery col-sm-4">
            ...
        </article>
    </div>
    <footer>
        <nav class="pagination clearfix">
            <!-- pagination goes here  -->
        </nav>
    </footer>
</div>
```

####10. Image Rotator
[Ref Image](http://dl.dropbox.com/u/33565642/iamge-rotator.png)

```html
<div class="content-block image-rotator-block" role="slider">
    <div class="slider-wrapper slider-id">
        <article>
            <figure><img src="#" alt="#slider-image"></figure>
            <aside>
                <h2><!-- slide title goes here  --></h2>
                <p>
                    <!-- slide content/description goes here -->
                </p>
                <a href="#" class="slider-btn btn">
                    <!-- button details goes here  -->
                </a>
            </aside>
        </article>
        <article>
            ...
        </article>
    </div>
</div>
```

####11. Recent Image Gallery Block
[Ref Image]()

```html
<div class="content-block latest-gallery-block">
    <ul class="latest-pics">
        <li>
            <a href="#linktopic" rel="lightbox">
                <img src="#" alt="#">
            </a>
        </li>
    </ul>
    <a class="view-all" href="#">VIEW ALL GALLERIES</a>
</div>
```

### Sidebars

####1. Image Tile
[Ref Image](http://dl.dropbox.com/u/33565642/image-tile.png)

```html
<div class="sidebar-block image-tile-block">
    <a href="#" title="#">
        <div class="image-tile-header">
            <figure><img class="img-responsive" src="#" alt="#"></figure>
            <h2> <!-- slider title goes here --></h2>
        </div>
        <div class="image-tile-caption">
            <!-- captions/desc goes here -->
        </div>
    </a>
</div>
```

####2. Child Page Menu
[Ref Image](http://dl.dropbox.com/u/33565642/child-sidebarmenu.png)

```html
<div class="sidebar-block sub-nav-block">
    <ul>
        <li><a href=""></a></li>
        <li> ... </li>
    </ul>
</div>
```

####3. Custom Content
[Ref Image](http://dl.dropbox.com/u/33565642/content_block.png)

```html
<div class="sidebar-block custom-content-block">
    <h3> <!-- title goes here  --> </h3>
    <div class="custom-content">
        <!-- content goes here -->
    </div>
</div>
```

####4. Event List Widget
[Ref Image](http://dl.dropbox.com/u/33565642/events-list.png)

```html
<div class="sidebar-block event-list-block">
    <h3> <!-- event widget title goes here  --> </h3>
    <article class="single-event">
        <div class="date">
            <!-- date and meta here  -->
        </div>
        <aside>
            <h4><a href="#"><!-- event title goes here  --></a></h4>
            <p>
                <!-- additional event info -->
            </p>
        </aside>
    </article>
    <article class="single-event">
        ...
    </article>
    <a href="#" class="view-all">VIEW ALL EVENTS</a>
</div>
```

####5. Latest Video
[Ref Image](http://dl.dropbox.com/u/33565642/latest-video.png)

```html
<div class=" sidebar-block latest-video">
    <h3> <!-- widget title goes here  --> </h3>
    <article class="single-video">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe src="#" frameborder="0"></iframe>
            <!-- videoembedded codes  -->
        </div>
        <aside>
            <a href="#player"> <!-- video title goes here  --> </a>
            <!-- video meta goes here  -->
        </aside>
    </article>
    <a href="#" class="view-all">VIEW ALL VIDEO</a>
</div>
````

####6. Recent Audio
[Ref Image](http://dl.dropbox.com/u/33565642/audio-widget.png)

```html
<div class="sidebar-block latest-audio-block clearfix">
    <div class="audio-articles">
        <article class="audio-article row">
            <a href="#" data-toggle="modal" data-target="#aud_clip_ID">
                <div class="icon-bg"><i class="fa fa-play-circle"></i></div>
                <aside>
                    <h4><!-- audio title goes here--></h4>
                    <p> <!-- audio title goes here--> </p>
                </aside>
            </a>
            <!-- audio popup player codes and scripts -->
        </article>
        <article class="audio-article row">
            ...
        </article>
    </div>
    <a href="#" class="view-all view-txt ">VIEW ALL AUDIO</a>
</div>
```

####7. Recent Blog Posts

```html
<div class="sidebar-block recent-post-block">
    <h3> <!-- widget title goes here --> </h3>
    <ul>
        <li><a href="#"><!-- post title --> </a> <span><!-- meta/desc if any --></span></li>
        <li> ... </li>
    </ul>
</div>
```

####8. Recent Gallery Block
[Ref Image](http://dl.dropbox.com/u/33565642/recent_image.png)

```html
<div class="sidebar-block latest-gallery-block">
    <ul class="latest-pics">
        <li>
            <a href="#linktopic" rel="lightbox">
                <img src="#" alt="#">
            </a>
        </li>
    </ul>
    <a class="view-all" href="#">VIEW ALL GALLERIES</a>
</div>
```


### Content Header

####1. Featured Events Block
[Ref Image](http://dl.dropbox.com/u/33565642/featred_events.png)

```html
<div class="content-header-block featured-events-block">
    <div class="banner-inner featured-events-slider" role="slider">
        <div class="events-slider slider-ID">
            <article class="single-event-slide">
                <figure><img src="#" alt="#eventIMG" /></figure>
                <aside>
                    <h1><!-- event title goes here --></h1>
                    <p>
                        <!-- event description goes here -->
                    </p>
                    <a href="#" class="slider-link event-link"> Read More</a>
                </aside>
            </article>
            <article class="single-event-slide">
                ...
            </article>
        </div>
    </div>
    <div class="featured-events-content">
        <div class="featured-events-block-top">
            <div class="featured-block-top-left">
                <h3><!-- Block title goes here --></h3>
                <!-- block description goes here  -->
            </div>
            <div class="featured-events-block-top-right">
                <a class="btn btn-general" href="#">Button Text</a>
                <!-- buttons codes  -->
            </div>
        </div>
    </div>
</div>
```

####2. Featured Video Carousel
[Ref Image](http://dl.dropbox.com/u/33565642/featured-video.png)

```html
<div class="content-header-block video-carousel-block">
    <div class="video-player-block featured-video">
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" name="player" id="player" src="#video-link" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="video-carosuel-content">
        <div class="video-carosuel-block-top">
            <div class="feature-block-top-left">
                <h3><!-- Block title goes here --></h3>
                <!-- block description goes here  -->
            </div>
            <div class="video-carosuel-block-top-right">
                <a class="btn btn-general" href="#">Button Text</a>
                <a href="#"> ... </a>
                <!-- buttons codes  -->
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="video-carousel-wrapper">
        <div id="video-carousel-slider" class="video-carosuelID">
            <article class="video-carousel-item">
                <div class="feature-video-box">
                    <a target="player" href="#" title="video-title">
                        <img src="#" alt="#video-thumb-url">
                    </a>
                    <span><!--Video title goes here --></span>
                </div>
            </article>
            <article class="video-carousel-item">
                ...
            </article>
        </div>
    </div>
</div>
```

####3. Custom Content
[Ref Image](http://dl.dropbox.com/u/33565642/content_block.png)

```html
<div class="content-header-block custom-content-block">
    <h3> <!-- title goes here  --> </h3>
    <div class="custom-content">
        <!-- content goes here -->
    </div>
</div>
```

####4. Image rotator
[Ref Image](http://dl.dropbox.com/u/33565642/iamge-rotator.png)

```html
<div class="content-header-block image" role="slider">
    <div class="slider-wrapper slider-id">
        <article>
            <figure><img src="#" alt="#slider-image"></figure>
            <aside>
                <h2><!-- slide title goes here  --></h2>
                <p>
                    <!-- slide content/description goes here -->
                </p>
                <a href="#" class="slider-btn btn">
                    <!-- button details goes here  -->
                </a>
            </aside>
        </article>
        <article>
            ...
        </article>
    </div>
</div>
```
####5. Header Image

### Content Footer

####1. Custom Content
[Ref Image](http://dl.dropbox.com/u/33565642/content_block.png)

```html
<div class="content-footer-block custom-content-block">
    <h3> <!-- title goes here  --> </h3>
    <div class="custom-content">
        <!-- content goes here -->
    </div>
</div>
```

####2. Footer Content Wells

```html
<section class="content-footer-block footer-content-well">
    <div class="footer-content-well-container container">
        <div class="content-well-row row">
            <article class="single-content-well widget-container col-sm-4">
                <div class="footer-well-widget">
                    <a href="#">
                        <img class="img-responsive" alt="#" src="#">
                        <aside class="footer-well-text">
                            <h3><!-- content well title goes here --> </h3>
                            <!-- content well description goes here -->
                        </aside>
                    </a>
                </div>
            </article>
            <article class="single-content-well widget-container col-sm-4">
                ...
            </article>
        </div>
    </div>
</section>
```

####3. Recent Image Gallery Block
[Ref Image](http://dl.dropbox.com/u/33565642/recent_image.png)

```html
<div class="content-block latest-gallery-block">
    <ul class="latest-pics">
        <li>
            <a href="#linktopic" rel="lightbox">
                <img src="#" alt="#">
            </a>
        </li>
    </ul>
    <a class="view-all" href="#">VIEW ALL GALLERIES</a>
</div>
```


### Common Modules

####1. Social icons

```html
<div class="social-icons">
    <ul>
        <li> <a target="_blank" href="#" title="#"><i class="fa  fa-networkname"></i></a> </li>
        <li> ...</li>
    </ul>
</div>
```

####2. Newsletter Subscription

```html
<form action="" method="POST" class="form-horizontal" role="form">
    <div class="form-group">
        <legend>Form title</legend>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
```

####3. Search Box

####4. Buttons

```html
<button type="button" class="btn btn-default">button</button>
<!-- button classes can add later using bootstrap sass mixins -->
```

####5. Bootstrap nav

```html
<div id="main-nav" class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            Main Menu <i class="fa fa-bars"></i>
        </button>
    </div>
    <!-- end hidden menu -->

    <nav class="collapse navbar-collapse" role="navigation">
        <ul class="nav navbar-nav">
            <li><a href="#">menu</a></li>
            <li>...</li>
        </ul>
    </nav>
</div>
```

####6. Logo

```html
<div class="site-logo">
  <a href="#" title="#>">
    <img src="#" alt="#" />
  </a>
</div>
```

####7. Navigations

```html
<div class="navbar">
    <h3><!-- menu title goes here --></h3>
    <ul class="nav navbar-nav">
        <li>
            <a href="#">Home</a>
        </li>
        <li> ... </li>
    </ul>
</div>
```

####8. Credit Block
